<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';


class Account extends REST_Controller 
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('gw_payview');
      
    }
	
	function ping_get()
	{
		$response            = [];
		$response['message'] = 'ping from Account class!';

		$this->response($response, 200);
	}


	function upload_post()
	{
		$file 	 = $_FILES['file'];
		$id      = trim($_POST['id']);
		$code 	 = 400;
		$message = "";

		try {
            if ($file['name'] != NULL && strlen($file['name']) > 0 && $id ){
				$config = [];
                $config['upload_path']   = '/var/www/html/uploads/';
                $config['allowed_types'] = 'jpg';
                $config['max_size']      = '1000'; // en kilobytes KB
                $config['max_width']     = '1024'; // pixeles
                $config['max_height']    = '768'; // pixeles
                $config['file_name']     = $id;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->overwrite = true;// para que reemplaze la imagen si tiene el mismo nombre
                if (!$this->upload->do_upload('file')) {
                    $message = $this->upload->display_errors();
                } else {
                	$code 	 = 200;
                	$message = "file loaded successfully ";
                    $data 	 = array('upload_data' => $this->upload->data());
                    $this->load->model('zdevtech');
		        	$response = $this->zdevtech->get($id);
		        	if ($response['code'] == 200) {
		        		$engine        						  = $response['message'];
			        	$engineRequest 						  = [];
                        $engineRequest 						  = $engine['engine'];
                  		$engineRequest['contact']['imageURL'] = "https://admin.costamaragencias.com/uploads/" . $id . '.jpg?';
                        $response = $this->zdevtech->save($engineRequest);
                        if ($response) {
                        	$message .= "!";
                        }
		        	}
                }
            } else {
            	$message = "Missing data";
            }
        } catch (Exception $e) {
            $message = $e->getTraceAsString();
        }

        $response = array( 'code' => $code, 'message' => $message);

        $this->response($response, $code); // 200 being the HTTP response code
	}



    /**
     * Comisiones facturadas de la agencia
     */
    function commissions_invoiced_get()
    {
        die("webservice out ofline");
        $code    = 200;

        if(!$this->get('id')){
            $this->response(NULL, 400);
        }
        $id                          = $this->get('id');
        $parameters                  = [];
        $parameters['id']            = $id;
        $parameters['itinerary_not'] = 'INCENTIVO C&B';

        $this->load->model('gw_payview');
        $items    = $this->gw_payview->getItemsProviderAGYDetail($parameters);
      
        $this->response(array('commissions' => $items), $code); // 200 being the HTTP response code

    }

    private function format($items, $total = 'total')
    {
        $totals = [];

        foreach ($items as $keyValues => $values) {
            $totals[] = $values[$total];
            foreach ($values as $key => $value) {
                $value = utf8_encode(trim($value));
                $items[$keyValues][$key] = $this->setDateFormat($key, $value);
            }
        }

        $response          = [];
        $response['total'] = array_sum($totals);
        $response['items'] = $items;

        return $response;

    }


    private function setDateFormat($key, $value)
    {
        if((strpos($key,'date') !== false) || (strpos($key, 'Date')) ){
            $date = new DateTime($value);
            $value = $date->format('d/m/Y');
        }
            return $value;
    }

    function commissions_uninvoiced_get()
    {
        die("webservice out ofline");
        $code    = 200;
        if(!$this->get('id')){
            $this->response(NULL, 400);
        }

        $id               = $this->get('id');
        $parameters                  = [];
        $parameters['id']            = $id;
        $parameters['ticketnum']     = '';
        $parameters['itinerary_not'] = 'INCENTIVO C&B';

        $this->load->model('gw_payview');
        $items = $this->gw_payview->getItemsProviderAGYDetail($parameters);
       

        $this->response(array('commissions' => $items), $code); // 200 being the HTTP response code
    }


    function your_debts_get()
    {
        die("webservice out ofline");
        $code    = 200;
        $message = [];

        if(!$this->get('id')){
            $this->response(NULL, 400);
        }
        $id               = $this->get('id');
        $parameters       = [];
        $parameters['id'] = $id;
        $this->load->model('gw_payview');
        $message    = $this->gw_payview->getSumDebtsByAccountIdDetail($parameters);
        
        if(!empty($message)){
            $message = $this->format($message, 'Debt');
        }

        $this->response(array( 'code' => $code, 'message' => $message), $code);
    }


    function items_on_account_negative_get()
    {
        die("webservice out ofline");
        $code    = 200;
        $message = [];
        if(!$this->get('id')){
            $this->response(NULL, 400);
        }
        $id               = $this->get('id');
        $parameters       = [];
        $parameters['id'] = $id;

        $this->load->model('gw_payview');
        $message    = $this->gw_payview->getItemsOnAccountNegative($parameters);
        if(!empty($message)){
            $message = $this->format($message, 'amount');
        }
        $this->response(array( 'code' => $code, 'message' => $message), $code);

    }


    function credit_approved_from_profile_get()
    {
        die("webservice out ofline");
        $code    = 200;
        $message = [];

        if(!$this->get('id')){
            $this->response(NULL, 400);
        }
        $id               = $this->get('id');
        $this->load->model('gw_account');
        $message   = $this->gw_account->getCreditApproved($id);

        $this->response(array( 'code' => $code, 'message' => $message), $code);
    }

    /**
     * Saber el tipo de credito de una agencia
     */
    function type_of_credit_get()
    {
        $id               = $this->get('id');
        $parameters       = [];
        $parameters['id'] = $id;

        // Ver que forma de calculo tiene la agencia
        $this->load->model('mongo_costamar');

        // type_of_credit = 1 -> credito manual
        // type_of_credit = 2 -> credito calculado
        $mongo_costamar_response = $this->mongo_costamar->find($parameters);

        // Por default todas las agencias tendran habilitado el credito calculado --------------------------------------
        $type_of_credit = 2;
        if (!is_null($mongo_costamar_response)) {
            $type_of_credit = $mongo_costamar_response['credit'];
        }

        $this->response(array( 'code' => 200, 'message' => $type_of_credit), 200);

    }

    
    function incentives_invoiced_get()
    {
        die("webservice out ofline");
        $this->load->model('gw_payview');
        
        $id               = $this->get('id');
        $parameters       = [];
        $parameters['id'] = $id;
		$parameters['itinerary'] = 'INCENTIVO C&B';
        $your_incentives 			   = $this->gw_payview->getItemsProviderAGYDetail($parameters);

        $this->response(array('incentives' => $your_incentives), 200);
    }

    function incentives_uninvoiced_get()
    {
        die("webservice out ofline");
        $this->load->model('gw_payview');
        
        $id               = $this->get('id');
        $parameters       = [];
        $parameters['id'] = $id;
        $parameters['itinerary'] = 'INCENTIVO C&B';
        $parameters['ticketnum'] = '';
        $your_incentives 			   = $this->gw_payview->getItemsProviderAGYDetail($parameters);

        $this->response(array('incentives' => $your_incentives), 200);
    }

    /***
     *  Recaudo Libre de un
     * 
     */
    function on_account_negative_not_applied_per_date_get()
    {           
        die("webservice out ofline");
        $parameters              = [];
        $parameters['id']        =  $this->get('id');
        $parameters['post_date'] = $this->get('post_date');
        
        $response = $this->gw_payview->getItemsOnAccountNegative($parameters);

        $this->response($response, 200);
    }
    
 

}