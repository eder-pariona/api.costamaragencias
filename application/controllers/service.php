<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';


class Service extends REST_Controller 
{

	/**
	 * @return 
	 */
	function account_get()
    {
		die("webservice out ofline");
        if(!$this->get('id')){
        	$this->response(NULL, 400);
        }
		$id                    = $this->get('id');
		$this->load->model('modulocobranzas_user');
		$parameters            = [];
		$parameters['id']      = $id;
		$response 			   = $this->modulocobranzas_user->get_account_executives($parameters);
		
		if( empty($response) ){
            $this->response(array( 'code' => 404, 'message' => 'Not Found'), 404);
		}

        $this->response(array( 'code' => 200, 'message' => $response), 200); // 200 being the HTTP response code

    }

    /**
	 * @return retorna total comisiones, pendiente de pago y credito disponible
	 * Local: curl --digest -u "admin:1234"  -i -X GET http://192.168.33.10/projects/codeigniter/codeigniter-restserver-2.7.0/index.php/service/account_commissions_debts_and_credit_available/id/0522256715/format/json
	 * Local: curl --digest -u "admin:1234"  -i -X GET http://192.168.33.10/costamar/costamaragencias/codeigniter/codeigniter-restserver-2.7.0/index.php/service/account_commissions_debts_and_credit_available/id/0601109540/format/json
	 * Local: curl --digest -u "admin:1234"  -i -X GET http://192.168.33.10/costamar/costamaragencias/codeigniter/codeigniter-restserver-2.7.0/index.php/service/account_commissions_debts_and_credit_available/id/0602991149/format/json
	 * Prod: curl --digest -u "clickandbook:NTgxMTcxODM0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.msq4EWYzgdBftrp1HhRbyoZxwCV3Cy7bSAD0PEjkcjRTsGudc7aWaay6MbtART95iIII0eTYTmhWUe7omCPSn-k_8dvjGQCDuclwQX30C9A15_22aTlWE_LOaGURs7dvMIe6YsZoD3bD1hoLEm51inlwC_ksIp9_VIif2nlZ1oXpxyFX4"  -i -X GET https://api.costamaragencias.com/service/account_commissions_debts_and_credit_available/id/0522256715/format/json
	 *
	 * El credito disponible puede ser una de estas 2 formas
	 * 1. Forma manual : se ingresa el monto en el profile de la agencia columna credit limint
	 * 2. Forma calculada: se sumara todas sus comisiones facturadas del presente año + more fee + sus onaccount negativos
	 * Una agencia puede tener solo 1 de estas formas 

	 */
    function account_commissions_debts_and_credit_available_get()
    {
		die("webservice out ofline");
    	if (!$this->get('id')){
        	$this->response(NULL, 400);
        }
		$this->load->model('gw_account');
		$this->load->model('gw_payview');
    	
    	// Get Accountid
		$id               = $this->get('id');
		$parameters       = [];
		$parameters['id'] = $id;
		$type_of_credit   = null;

		// Nos Deben ---------------------------------------------------------------------------------------------------
		$response_debts    = $this->gw_payview->getSumDebtsByAccountId($parameters);
		$your_debts 	   = 0.00;
		if(!empty($response_debts)){		
			$your_debts =  str_replace(',', '', $response_debts->Debt);
			if ($your_debts < 0) {
				$your_debts = 0.00;
			}
		}

		$amounts_favor_of_the_agency = [];
	
		// Comisiones facturadas a favor de la agencia
		// Pero no queremos los incentivos
		$parameters['itinerary_not_equal'] = 'INCENTIVO C&B';
		$amount_provider_agy 		   = $this->gw_payview->getItemsProviderAGY($parameters)->amount;
		$amounts_favor_of_the_agency[] = $amount_provider_agy;

		// Saldos o pagos adelantados a favor de la agencia
		//$items_on_account_negative     = $this->gw_payview->getItemsOnAccountNegative($parameters);

		$items_on_account_negative     = $this->gw_payview->getOnAccountNegatives($parameters);
		
		$your_on_account_negative      = $this->getTotals($items_on_account_negative, 'amount');
		$amounts_favor_of_the_agency[] = $your_on_account_negative;


		unset($parameters['itinerary_not_equal']); 
		// Comisiones Incentivos facturados a favor de la agencia
		$parameters['itinerary_equal'] = 'INCENTIVO C&B';
		$your_incentives 			   = $this->gw_payview->getItemsProviderAGY($parameters)->amount;
		$amounts_favor_of_the_agency[] = $your_incentives;

		if ($your_incentives != 0.00) {
			$your_incentives = $your_incentives * -1;
		}

		if ($amount_provider_agy != 0.00) {
			$amount_provider_agy = $amount_provider_agy * -1;

		}
		if (isset($your_on_account_negative)) {
			$your_on_account_negative = $your_on_account_negative * -1;
		}

		$account_approved_credit_calculated_sum = array_sum($amounts_favor_of_the_agency);
		$account_approved_credit_calculated_sum = $account_approved_credit_calculated_sum * -1;

		$your_credit_approved          = $account_approved_credit_calculated_sum;
		$your_credit_approved_totals   = [];
		$your_credit_approved_totals[] = $your_credit_approved;

		// Traemos el credito manual a favor del account
		$your_credit_manual            = $this->gw_account->getCreditApproved($id);
		if ($your_credit_manual > 0) {
			$your_credit_approved_totals[] = $your_credit_manual;
		}

		$your_credit_approved 	 = array_sum($your_credit_approved_totals);
		$agency_credit_available =  $your_credit_approved - $your_debts;
		
		$your_credit_available   = 0.00;
		if ($agency_credit_available > 0) {
			$your_credit_available = $agency_credit_available;
		}
		$we_owe_you = $amount_provider_agy;

		$return                             = [];
		$return['account_id'] 			    = $id;
		$return['your_debts'] 			    = floatval($your_debts); // las deudas cash y las emitidas con la tarjeta de costamar
		$return['we_owe_you'] 			    = floatval($we_owe_you); // las comisiones y extra fee a favor de la agencia pero la agencia tiene que presentar factura
		$return['your_credit_available'] 	= floatval($your_credit_available);  // la resta entre el your_credit - your_debts
		$return['your_on_account_negative'] = floatval($your_on_account_negative);
		$return['your_manual_credit']       = floatval($your_credit_manual);
		$return['your_incentives']			= floatval($your_incentives);
		$return['type_of_credit']			= null;
		
        $this->response(array( 'code' => 200, 'message' => $return), 200);
    }



    private function getTotals($items, $column)
    {
    	$totals = [];
    	foreach ($items as $key => $value) {
    		$totals[] = $value[$column];
    	}

    	return array_sum($totals);
    }


   	/**
	 * @return Linea de Credito de un account
	 * puede ser de 2 tipos calculada y manual
	 * esta funcion se usa en el emite facil del costamaragencias
	 */
    function account_credit_line_get()
     {
		die("webservice out ofline");
     	if(!$this->get('id')){
         	$this->response(NULL, 400);
         }
 		$id               = $this->get('id');
 		$this->load->model('gw_account');
 		$this->load->model('gw_payview');
 		$parameters       = [];
 		$parameters['id'] = $id;
 
 		$your_debts 	  = 0.00;
 		$your_credit_used = 0.00;
 
 		// Deudas del account
 		$response_debts    = $this->gw_payview->getSumDebtsByAccountId($parameters);
		 
 		if(!empty($response_debts)){		
 			$your_debts =  str_replace(',', '', $response_debts->Debt);
 			if ($your_debts < 0) {
 				$your_debts = 0.00;
 			}		
 		}
 
 		$your_credit_approved_totals = [];
 		// Credito manual de la agencia
 		$your_manual_credit  		   = $this->gw_account->getCreditApproved($id);
		if ($your_manual_credit > 0) {
			$your_credit_approved_totals[] = $your_manual_credit; 
		}
 		// El credito es calculado
 		$account_approved_credit_calculated     = $this->account_approved_credit_calculated_get($parameters);
 		$your_commissions_invoiced 				= $account_approved_credit_calculated[0] * -1;
 		$your_on_account_negative  				= $account_approved_credit_calculated[1] * -1;
 
 		$account_approved_credit_calculated_sum = array_sum($account_approved_credit_calculated);
 		$your_credit_approved                   = $account_approved_credit_calculated_sum * -1;
 		$your_credit_approved_totals[] 			= $your_credit_approved; 
 
 		$your_credit_approved = array_sum($your_credit_approved_totals);
 		
 		$your_credit_available =  $your_credit_approved - $your_debts;
 		if ($your_credit_available < 0 ) {
 			$your_credit_used = $your_credit_approved;
 		}
 
 		if( $your_credit_available < 0 ){
 			$your_credit_available = 0.00;
 		}
 
 		$return                          = [];
 		$return['your_debts']            = number_format(floatval($your_debts), 2);
 		$return['your_credit_approved']  = number_format(floatval($your_credit_approved), 2);
 		$return['your_credit_available'] = number_format(floatval($your_credit_available), 2);
 		$return['your_credit_used']      = number_format(floatval($your_credit_used), 2);
 		$return['your_manual_credit']        = $your_manual_credit;
 		$return['your_commissions_invoiced'] = $your_commissions_invoiced;
 		$return['your_on_account_negative']  = $your_on_account_negative;
 
 
         $this->response(array( 'code' => 200, 'message' => $return), 200);
 
     }



    function account_approved_credit_calculated_get($parameters = array())
    {
		die("webservice out ofline");
		$this->load->model('gw_payview');
		$agency_credits = [];

		// Comisiones de la agencia y fee que le pertenecet --------------------------------------
		$response_items_provider_agy = $this->gw_payview->getItemsProviderAGY($parameters);
		$amount_provider_agy 		 = $response_items_provider_agy->amount;
		$agency_credits[] 			 = $amount_provider_agy;

		// On account negativo----------------------------------------------------------------------
		$response_items_on_account_negative = $this->gw_payview->getOnAccountNegatives($parameters);
		$amount_on_account_negative = $this->getTotals($response_items_on_account_negative, 'amount');
		$agency_credits[] = $amount_on_account_negative;



		return $agency_credits;
    }

}
