<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';


class Refund extends REST_Controller 
{

	// curl -i -X POST  -d 'ticketnum=6005728530'  http://api.costamaragencias.com/index.php/refund/search/format/json
	function search_post()
    {
    	$search      = false;
		$parameters  = [];
    	$response    = [];
    	if(!empty($_POST)){
    		foreach ($_POST as $key => $value) {
    			$parameters['where'] = $key . ' = \'' . trim($value) . '\'';
    		}
    	}
		$this->load->model('gw_refund');
		$response    = $this->gw_refund->getInvoices($parameters);

        // Format 
        foreach ($response as $key => $value) {
            $dataTktAgt = trim($value->dataTktAgt);
            $whatsapp = '';
            if ($dataTktAgt) {
                $explode = explode('/', $dataTktAgt);
                if (is_array($explode)) {
                    $whatsapp = $explode[0];
                }
            }
            $response[$key]->whatsapp = $whatsapp;
        }

		$this->response(array( 'code' => 200, 'message' => $response), 200);

    }
}


