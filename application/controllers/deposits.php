<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Deposits extends REST_Controller
{
   /**
	*
	* curl --digest -u "admin:1234" -i -X GET http://192.168.33.10/costamar/costamaragencias/codeigniter/codeigniter-restserver-2.7.0/index.php/deposit/ping
	*
	*/
	function ping_get()
	{
		$response            = [];
		$response['message'] = 'ping from Deposit class!';

		$this->response($response, 200);
	}

	/**
	 *
	 * curl --digest -u "admin:1234" -i -X GET http://192.168.33.10/costamar/costamaragencias/codeigniter/codeigniter-restserver-2.7.0/index.php/deposit/find/account/0488028180/from/2019-11-01/to/2019-12-19/format/json
	 * Local
	 * curl --digest -u "admin:1234" -i -X GET http://192.168.33.10/costamar/costamaragencias/codeigniter/codeigniter-restserver-2.7.0/index.php/deposits/find/account/9999911111/from/2019-12-01/to/2019-12-19/format/json

	 * Prod: curl --digest -u "clickandbook:NTgxMTcxODM0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.msq4EWYzgdBftrp1HhRbyoZxwCV3Cy7bSAD0PEjkcjRTsGudc7aWaay6MbtART95iIII0eTYTmhWUe7omCPSn-k_8dvjGQCDuclwQX30C9A15_22aTlWE_LOaGURs7dvMIe6YsZoD3bD1hoLEm51inlwC_ksIp9_VIif2nlZ1oXpxyFX4"  -i -X GET https://api.costamaragencias.com/deposits/find/account/0488028180/from/2019-11-01/to/2019-12-04/format/json
	 * 
	 */
	function find_get() 
	{
		$id       = $this->get('account');
		$from     = $this->get('from');
		$to       = $this->get('to');
		$amount   = $this->get('amount');
		$code     = 404;
		$message  = [];

		if ($id && $from && $to) {
			$code = 200;
			$this->load->model('deposit');
	        $response = $this->deposit->getDeposits($id, $from, $to, $amount);
	        if($response->num_rows > 0){
	            foreach ($response->result() as $row) {
	            	$row->Traveler = utf8_encode(trim($row->Traveler));
	                $message[] = $row;
	            }
	        }
		}

		$response = [
			'code' => $code,
			'message' => $message,
		];

        $this->response($response, $code);

	}

}