<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Location extends REST_Controller
{
	/**
	 * @return
	 * local
	 * $ curl --digest -u "admin:1234" -i -X GET http://192.168.33.10/projects/codeigniter/codeigniter-restserver-2.7.0/index.php/promotion/last_five/refresh/0/format/json
	 * prod
	 * $ curl --digest -u "clickandbook:NTgxMTcxODM0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.msq4EWYzgdBftrp1HhRbyoZxwCV3Cy7bSAD0PEjkcjRTsGudc7aWaay6MbtART95iIII0eTYTmhWUe7omCPSn-k_8dvjGQCDuclwQX30C9A15_22aTlWE_LOaGURs7dvMIe6YsZoD3bD1hoLEm51inlwC_ksIp9_VIif2nlZ1oXpxyFX4" -i -X GET https://api.costamaragencias.com/promotion/last_five/format/json
	 */
	
	function ping_get()
    {
		$response            = [];
		$response['code']    = 200;
		$response['message'] = 'Hello world';

        $this->response($response, 200); // 200 being the HTTP response code

    }
    /**
     * local curl --digest -u "admin:1234" -i -X GET http://192.168.33.10/costamar/costamaragencias/codeigniter/codeigniter-restserver-2.7.0/index.php/location/lookup/ip/192.168.1.10/format/json
     * production:
     * curl --digest -u "clickandbook:NTgxMTcxODM0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.msq4EWYzgdBftrp1HhRbyoZxwCV3Cy7bSAD0PEjkcjRTsGudc7aWaay6MbtART95iIII0eTYTmhWUe7omCPSn-k_8dvjGQCDuclwQX30C9A15_22aTlWE_LOaGURs7dvMIe6YsZoD3bD1hoLEm51inlwC_ksIp9_VIif2nlZ1oXpxyFX4"  -i -X GET https://api.costamaragencias.com/location/lookup/ip/190.81.61.199/format/json
     */
    function lookup_get()
    {
		$dirname        = dirname(__FILE__);
		
		$dirname_models = $dirname . '/../models/database';
    	require_once($dirname_models . '/IP2Location.php');
    	$ip = $this->get('ip');
    	$database = 'IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE-TIMEZONE-ISP-DOMAIN-NETSPEED-AREACODE-WEATHER-MOBILE-ELEVATION.BIN';
    	$ip2location = new IP2Location($dirname_models . '/' . $database);
    	$response = $ip2location->lookup($ip, IP2Location::ALL);

    	$this->response($response, 200);

    }


}
