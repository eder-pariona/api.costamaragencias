<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';


class Reimbursement extends REST_Controller 
{

	// curl -i -X POST  -d 'ticketnum=6005728530'  http://api.costamaragencias.com/reimbursement/search/format/json
	// ticketnum 4718596806   
	/*
	 * Para el caso del Costamar.com
	 * Si el estado es PENDIENTE o POR PAGAR A LA AGENCIA:
	 *  - Si la forma de pago es Cash, mostrar el monto (PorPagar - RefFee/CantRef)
	 * Si el estado es PAGADO A LA TARJETA
	 *  - Se debe mostrar los montos separados  
	      PorPagar (que estará en 0) , RefFee/CantRef (esto es el Fee por el reembolso)
	 * Si el estado es PAGADO AL CLIENTE
	 *  - Se debe mostrar ((TotalCost*-1) - RefFee/CantRef)
	 */
	/*
		ticketnum: 3157068012, POR PAGAR AL CLIENTE     
	*/
	function search_post()
    {
		$parameters  = [];
    	$response    = [];
    	if(!empty($_POST)){
    		foreach ($_POST as $key => $value) {
    			$parameters['where'] = $key . ' = \'' . trim($value) . '\'';
    		}
    	}
		$this->load->model('gw_refund');
		$response    = $this->gw_refund->getRefunds($parameters);
        // Format 
        $toPrint = [];
        foreach ($response as $key => $value) {
        	$status = trim($value->Estado);
        	$fop    = trim($value->FP);
        	/*
        	- Si el estado es PENDIENTE o POR PAGAR A LA AGENCIA:
      			. Si la forma de pago es Cash, mostrar el monto (PorPagar - RefFee/CantRef)
			*/
      		$haystack = [ 'PENDIENTE', 'POR PAGAR AL CLIENTE' ];
      		$quotient = $response[$key]->RefFee / $response[$key]->CantRef; 
      		$response[$key]->quotient = $quotient;
      		$type = 0;
      		$toPay = $response[$key]->PorPagar;
      		if (in_array($status, $haystack)) {
      			$type = 1;
      			if ($fop == 'C') {
      				$toPay = ($response[$key]->PorPagar) - $quotient;
      			}
      		} elseif ($status == 'PAGADO A LA TARJETA') {
      			$type = 2;
      			// Se debe mostrar los montos separados  PorPagar (que estará en 0) , RefFee/CantRef (esto es el Fee por el reembolso)
      			// ticketNum: 3704958196, 3704958197, 3704958217, 3704958218,
      		
      		} elseif ($status == 'PAGADO AL CLIENTE') {
      			$type = 3;
      			// Se debe mostrar ((TotalCost*-1) - RefFee/CantRef)
      			$toPay = ($response[$key]->Totalcost * -1) - $quotient;
      		}

      		$response[$key]->PorPagar = number_format($toPay, 2);
      		$response[$key]->type     = $type;
        }
    	// echo "<pre>",__FILE__." on line ".__LINE__,": "; exit(); echo "</pre>"; 

		$this->response(array( 'code' => 200, 'message' => $response), 200);

    }
}


