<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';


class Refundusa extends REST_Controller 
{

	function searchone_post()
	{	
		$parameters = [];
		$type	   = $_POST['type'];
    	$search	   = $_POST['search'];

    	$parameters  = [];
    	$parameters['where'] = " AND inv.invoicenumber = '" . $search . "'";
    	if ($type == 2){
    		$parameters['where'] = " AND inv.pnrlocator = '" . $search . "'";
    	} elseif($type === 3) {
    		$parameters['where'] = " AND inv.ticketNum = '" . $search . "'";
    	}


		$this->load->model('gw_usa_refund');
		$response    = $this->gw_usa_refund->getinvoices($parameters);
		
		$this->response(array( 'code' => 200, 'message' => $response), 200);

	}



	// curl -i -X POST  -d 'ticketnum=6005728530'  http://api.costamaragencias.com/index.php/refundusa/search/format/json
	// Pendiente de Respuesta de la Aerolinea = acc: 3239809066 invoice: 970417549, tickt: 7441994500
	// Pendiente de Pago = acc: 3234012076 invoice: 970406075, tickt: 7337054533          
	// Pagado = acc: 5747320632 invoice: 001086962, tickt: 7485512781
	// TRAMITADO = acc: 9732723889 invoice: 580465309, tickt: 7326029944          
	//           
	function search_post()
    {
    	// echo "<pre>",__FILE__." on line ".__LINE__,": "; print_r($_POST); echo "</pre>"; 
    	// echo "<pre>",__FILE__." on line ".__LINE__,": "; exit(); echo "</pre>"; 
    	$accountId = $_POST['accountId'];
    	$type	   = $_POST['type'];
    	$search	   = $_POST['search'];
    	// type 1 = invoicenumber
    	// type 2 = pnr

		$parameters  = [];
    	$parameters['where'] = " AND inv.invoicenumber = '" . $search . "'";
    	if ($type == 2){
    		$parameters['where'] = " AND inv.pnrlocator = '" . $search . "'";
    	}
		$parameters['accountId'] = $accountId;

    	// echo "<pre>",__FILE__." on line ".__LINE__,": "; print_r($parameters); echo "</pre>"; 
    	// $parameters = [];
		$this->load->model('gw_usa_refund');
		$response    = $this->gw_usa_refund->getRefunds($parameters);
		// echo "<pre>",__FILE__." on line ".__LINE__,": "; print_r($response); echo "</pre>"; 
  //   	echo "<pre>",__FILE__." on line ".__LINE__,": "; exit(); echo "</pre>"; 
		$this->response(array( 'code' => 200, 'message' => $response), 200);

    }
}


