<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';


class Payment extends REST_Controller 
{

	/**
	 * @return 
	 */
	function show_get()
    {
        die("webservice out ofline");
        $this->load->model('gw_payments');
        $pnr = $this->get('pnr');
        $parameters       = [];
        $parameters['pnr'] = $pnr;
        $response = $this->gw_payments->getByParameters($parameters);


        $this->response($response, 200); // 200 being the HTTP response code

    }
}