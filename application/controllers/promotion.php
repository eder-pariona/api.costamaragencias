<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Promotion extends REST_Controller
{
	/**
	 * @return
	 * local
	 * $ curl --digest -u "admin:1234" -i -X GET http://192.168.33.10/projects/codeigniter/codeigniter-restserver-2.7.0/index.php/promotion/last_five/refresh/0/format/json
	 * prod
	 * $ curl --digest -u "clickandbook:NTgxMTcxODM0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.msq4EWYzgdBftrp1HhRbyoZxwCV3Cy7bSAD0PEjkcjRTsGudc7aWaay6MbtART95iIII0eTYTmhWUe7omCPSn-k_8dvjGQCDuclwQX30C9A15_22aTlWE_LOaGURs7dvMIe6YsZoD3bD1hoLEm51inlwC_ksIp9_VIif2nlZ1oXpxyFX4" -i -X GET https://api.costamaragencias.com/promotion/last_five/format/json
	 */
	
	function last_five_get()
    {
    	$refresh = false;
    	if($this->get('refresh')){
    		$refresh = true;
        }

		$memcache_obj = new Memcache;
		$memcache_obj->connect('localhost', 11211);
		$cachename    = 'costamaragencias_promotions';
		
    	if( $refresh ){
	    	// delete key memcache
			memcache_delete($memcache_obj, $cachename);
    	}
    	
		$format = [];
		$data = $message = '';
		if(	empty(memcache_get($memcache_obj, $cachename))){
			$host     = 'costamardotcom.coqgwhzv9gw6.us-west-2.rds.amazonaws.com';
			$database = 'costamar_agencias';
			$username = 'root';
			$password = '63wTMdV$Cu&)E]#F';
			$conn = new PDO('mysql:host='.$host.';dbname='.$database, $username, $password);
			$query = 'SELECT SQL_CALC_FOUND_ROWS ca_2_posts.ID, ca_2_posts.post_title, ca_2_posts.post_name, ca_2_posts.post_content FROM ca_2_posts
			    INNER JOIN ca_2_term_relationships
			    ON (ca_2_posts.ID = ca_2_term_relationships.object_id)
			    WHERE 1=1 AND ( ca_2_term_relationships.term_taxonomy_id IN (5) )
			    AND ca_2_posts.post_type = "post" AND (ca_2_posts.post_status = "publish")
			    GROUP BY ca_2_posts.ID ORDER BY ca_2_posts.post_date DESC LIMIT 0, 16
			  ';
			$query = $conn->prepare($query);
			$query->execute();
			$query->setFetchMode(\PDO::FETCH_ASSOC);
			$response = $query->fetchAll();
			if (!empty($response)){
				//
				$collection = [];
				foreach ($response as $key => $value) {
					if ($value['ID'] == 15561){
						$collection = $response[$key];
						unset($response[$key]);
						break;
					}
				}
				//
				if (!empty($collection)){
					array_unshift($response, $collection);
				}
				$count = 0;
				foreach ($response as $key => $post){
					$response[$key]['post_title'] = utf8_encode($post['post_title']);
					$response[$key]['post_content'] = utf8_encode($post['post_content']);
					$query = "SELECT meta_value FROM ca_2_postmeta WHERE post_id = :post_id AND meta_key = 'ibanner'";
					$query = $conn->prepare($query);
					$query->bindParam(":post_id", $post['ID']);
					$query->execute();
					$query->setFetchMode(\PDO::FETCH_OBJ);
					$banner = $query->fetchObject();
					if( $banner ){
						$response[$key]['meta_value'] = $banner->meta_value;
					}
					$format[$post['post_name']] = $response[$key];
				}
				$data = json_encode($format);
				// 300 : 5 minutos se renueva la cache
				// 3600 : 1 hour
				memcache_set($memcache_obj, $cachename, serialize($data), 0, 3600);
				$message = 'Loaded from the database ';
			} 
		} else {
			$data    = unserialize(memcache_get($memcache_obj, $cachename));
			$message = 'Loaded from the cache';
		}

		$code              = 200;
		$message           = $message;

		$return            = [];
		$return['code']    = $code;
		$return['message'] = $message;
		$return['data']    = $data;
		

        $this->response($return, 200); // 200 being the HTTP response code

    }

 


}
