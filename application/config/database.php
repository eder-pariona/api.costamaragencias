<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = 'costamardotcom.coqgwhzv9gw6.us-west-2.rds.amazonaws.com';
$db['default']['username'] = 'root';
$db['default']['password'] = '63wTMdV$Cu&)E]#F';
$db['default']['database'] = 'costamaragency';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$db['modulocobranzas']['hostname'] = 'costamardotcom.coqgwhzv9gw6.us-west-2.rds.amazonaws.com';
$db['modulocobranzas']['username'] = 'root';
$db['modulocobranzas']['password'] = '63wTMdV$Cu&)E]#F';
$db['modulocobranzas']['database'] = 'costamar_cobranzas';
$db['modulocobranzas']['dbdriver'] = 'mysql';
$db['modulocobranzas']['dbprefix'] = '';
$db['modulocobranzas']['pconnect'] = TRUE;
$db['modulocobranzas']['db_debug'] = TRUE;
$db['modulocobranzas']['cache_on'] = FALSE;
$db['modulocobranzas']['cachedir'] = '';
$db['modulocobranzas']['char_set'] = 'utf8';
$db['modulocobranzas']['dbcollat'] = 'utf8_general_ci';
$db['modulocobranzas']['swap_pre'] = '';
$db['modulocobranzas']['autoinit'] = TRUE;
$db['modulocobranzas']['stricton'] = FALSE;


$db['globalperu']['hostname'] = 'costamarperu';
$db['globalperu']['username'] = 'global';
$db['globalperu']['password'] = 'ware';
$db['globalperu']['database'] = 'costamarperu';
$db['globalperu']['dbdriver'] = 'odbc';
$db['globalperu']['dbprefix'] = '';
$db['globalperu']['pconnect'] = TRUE;
$db['globalperu']['db_debug'] = TRUE;
$db['globalperu']['cache_on'] = FALSE;
$db['globalperu']['cachedir'] = '';
$db['globalperu']['char_set'] = 'utf8';
$db['globalperu']['dbcollat'] = 'utf8_general_ci';
$db['globalperu']['swap_pre'] = '';
$db['globalperu']['autoinit'] = TRUE;
$db['globalperu']['stricton'] = FALSE;


$db['globalusa']['hostname'] = 'costamarusa';
$db['globalusa']['username'] = 'global';
$db['globalusa']['password'] = 'ware';
$db['globalusa']['database'] = 'costamarusa';
$db['globalusa']['dbdriver'] = 'odbc';
$db['globalusa']['dbprefix'] = '';
$db['globalusa']['pconnect'] = TRUE;
$db['globalusa']['db_debug'] = TRUE;
$db['globalusa']['cache_on'] = FALSE;
$db['globalusa']['cachedir'] = '';
$db['globalusa']['char_set'] = 'utf8';
$db['globalusa']['dbcollat'] = 'utf8_general_ci';
$db['globalusa']['swap_pre'] = '';
$db['globalusa']['autoinit'] = TRUE;
$db['globalusa']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */