<?php 
class Gw_account extends CI_Model
{
	public function __construct() {
        parent::__construct();
        $this->_db = $this->load->database('globalperu', TRUE);
    }

	public function ping()
	{
		echo "<pre>",__FILE__." on line ".__LINE__,": "; exit(); echo "</pre>"; 
	}

    /**
     * Obtengo todas las deudas de la agencia y le resto las comisiones
     * Solo obtiene las deudas cash pendiente de pago
     * Solo obtiene las deudas plastic pendiente de pago pero emitidas con las tarjetas de costamar (122093_1501, 378790_1001)
     */
	public function getDebts($parameters)
	{
		$account_id      = $parameters['id'];
		$currentYear    = date('Y');
		$currentYearAnt = $currentYear - 1;
		$query = "
		SELECT  pv.AccountId,
                cu.Name, 
                Debt= SUM(ISNULL(
                    (CASE pv.Fop
                        WHEN 'P' THEN (
                            CASE
                                WHEN ((SELECT TOP 1 invoi.InvoiceNumber FROM dba.Invoice invoi 
                                    WHERE (
                                        invoi.CCNumber LIKE '378790_____2009' 
                                        OR invoi.CCNumber LIKE '122093_____1501' 
                                        OR invoi.CCNumber LIKE '491947_____7155' 
                                        OR invoi.CCNumber LIKE '378790_____1001')
                                    AND invoi.InvoiceNumber = pv.InvoiceNumber) = pv.InvoiceNumber)
                                 THEN
                                    (
                                        SELECT TOP 1 (invo.TotalCost - payc.CustRcvdAmt) FROM dba.payments payc 
                                        INNER JOIN dba.Invoice invo with(nolock) ON invo.AccountId = payc.Provider 
                                        WHERE payc.[comment] like pv.invoiceNumber + '/' + pv.traveler + '%'
                                        AND invo.fop = 'P' 
                                        AND invo.InvoiceNumber = pv.InvoiceNumber  
                                        AND payc.CustRcvdAmt <> invo.TotalCost 
                                    )
                                ELSE 0
                            END
                        )
                        WHEN 'C' THEN (pv.CustDueAmt-pv.CustRcvdAmt)
                     END), 0)
                    ),
                    cu.CreditLimitAmount
                FROM dba.PayView pv WITH(nolock), trwuser.CustomerView cu WITH(nolock) 
                WHERE cu.AccountId = pv.AccountId
                AND pv.ACCOUNTID = '$account_id'
                AND pv.provider <> pv.AccountID
                AND (pv.STATUS <> 'V' OR pv.STATUS IS NULL) and year(pv.custduedate)>= $currentYearAnt
                AND ( (pv.PROVIDER NOT IN ('AGY','AGENCY') ) or 
                    (pv.PROVIDER IN ('AGY','AGENCY') and year(pv.custduedate) = $currentYear and (Select top 1 fop from dba.invoice where invoiceNumber = pv.invoiceNumber and accountID = '$account_id' and itinerary not like '%Over%' and status <> 'V' order by fop desc) = 'C')
                 )
                AND pv.provider <> 'DCT'
                AND cu.Name not like '(X)%'
                AND pv.id not in (Select inv.payid from dba.invoice inv with(nolock) where inv.payid = pv.id 
                AND inv.Settle + inv.TravelType = 'IA' 
                AND ISNULL(inv.docType,'') = 'REF' 
                AND inv.apDueDate in ('2019-06-30','2019-12-31') )
                GROUP BY pv.AccountId,
                        cu.Name,
                        cu.CreditLimitAmount,
                        cu.CustType";

		$response = $this->_db->query($query);
		
		return $response->row();

	}

	public function getCommissions($parameters)
	{
		$account_id = $parameters['id'];
		$commission = 0;
		$query      = "SELECT
                    pv.CustDueAmt-pv.CustRcvdAmt as 'Total',
                    inv.TicketNum as commissionInvoice,
                    inv.InvoiceDate,
                    inv.TotalCost,
                    inv.InvoiceNumber
                            From
                    dba.PayView pv with(nolock)
                            inner join  dba.invoice inv with(nolock)
                        on  pv.id= inv.payid
                    where
                            ((  pv.Status <> 'V')
                                    or pv.invoiceDate is null
                            )
                        and pv.AccountID = '$account_id'
                        and pv.CustRcvdFlag <> 'X'
                        and inv.saletype = 'R'
                        and inv.settle = 'I'
                        and inv.traveltype = 'F'
                        and inv.fop = 'C'
                        and inv.provider in ('AGY', 'DCT')
                        and inv.totalCost < 0
                        AND inv.Branch NOT LIKE '77%'
                ";
		$response = $this->_db->query($query);
		$records  = $response->result();
		if(!empty($records)){
            foreach ($records as $row) {
                $response = $this->checkCommisionDefeated($row);
                if(!$response) {
                    $commission = $commission + $row->Total;
                }
            }
		}
		$this->_db->close();

		return $commission;
	}

    /*
     * Obtener el credito actual de la agencias, es decir cuanto le quedan despues de haber hecho o no emisiones
     * Primera version: Solo consulta las emisiones cash
     * Seguna version: tambien consulta las emisiones plastic pero las pagadas con las tarjetas de costamar:
     * 122093_1501
     * 378790_1001
     */
	public function getCreditAvailable($parameters)
	{
        $account_id = $parameters['id'];
        $credit     = 0.00;
        $query      = "SELECT 
				CV.AccountID,
				CV.Name, 
				CreditLimitAmount = ISNULL(( SELECT ( IF cv.CreditLimitAmount <= 0 THEN 0 
                                                     ELSE  ( cv.CreditLimitAmount -  SUM(ISNULL(
                                                            (CASE pv.Fop
                                                                WHEN 'P' THEN (
                                                                    CASE
                                                                        WHEN ((SELECT TOP 1 invoi.InvoiceNumber FROM dba.Invoice invoi 
                                                                            WHERE (invoi.CCNumber LIKE '378790_____2009' OR 
                                                                                invoi.CCNumber LIKE '122093_____1501' OR 
                                                                                invoi.CCNumber LIKE '491947_____7155' OR 
                                                                                invoi.CCNumber LIKE '378790_____1001' OR
                                                                                invoi.CCNumber LIKE '491947______5492')
                                                                            AND invoi.InvoiceNumber = pv.InvoiceNumber) = pv.InvoiceNumber)
                                                                         THEN
                                                                            (
                                                                                SELECT TOP 1 (invo.TotalCost - payc.CustRcvdAmt) FROM dba.payments payc 
                                                                                INNER JOIN dba.Invoice invo with(nolock) ON invo.AccountId = payc.Provider 
                                                                                WHERE payc.[comment] like pv.invoiceNumber + '/' + pv.traveler + '%'
                                                                                AND invo.fop = 'P' 
                                                                                AND invo.InvoiceNumber = pv.InvoiceNumber 
                                                                            )
                                                                        ELSE 0
                                                                    END
                                                                )
                                                                WHEN 'C' THEN (pv.CustDueAmt-pv.CustRcvdAmt)
                                                             END), 0)
                                                            )) 
                                                           ENDIF)
				FROM dba.PayView pv WITH(NOLOCK) WHERE  cv.AccountId=pv.AccountId 
				AND ( year(pv.InvoiceDate) = year(getdate())
                      AND pv.Status <> 'V' 
                      OR  pv.invoiceDate is null ) 
				AND invoicenumber <> 'ONACCOUNT' 
                AND pv.Branch NOT LIKE '77%'
                AND pv.id not in (
                    Select inv.payid from dba.invoice inv with(nolock)
                    where inv.payid = pv.id
                    AND inv.Settle + inv.TravelType IN ('IA', 'AA')
                    AND ISNULL(inv.docType,'') = 'REF'
                    )
                AND pv.id not in (
                    SELECT inv.payid FROM dba.invoice inv with(nolock)
                    WHERE inv.payid = pv.id
                    and inv.Itinerary IN  ('PAGOCOMMPERU', 'SAFETYPAY')
                    AND inv.Provider = 'AGY'
                )
                ), CV.CreditLimitAmount)
				FROM TRWUSER.CustomerView CV with(nolock)  Where CV.ACCOUNTID = '$account_id' ";

		$response = $this->_db->query($query);
		$records  = $response->row();
		if(!empty($records)){
			$credit = $records->CreditLimitAmount;
		}

		return $credit;
	}


    /**
     * Credito ingresado manual en el profile del account
     */
    public function getCreditApproved($account_id)
    {
        $query = "SELECT ISNULL((CASE WHEN (creditLimitAmount < 0 OR creditLimitAmount = 1) THEN creditLimitAmount ELSE creditLimitAmount end), 0)";
        $query .= " AS 'creditApproved' FROM TRWUSER.CustomerView WHERE accountid = '$account_id' ";

        $response = $this->_db->query($query);
        $records  = $response->row();
        $credit_approved = 0.00;
        if(!empty($records)){
            $credit_approved = $records->creditApproved;
        }
        $this->_db->close();

        return $credit_approved;
    }


	/**
	* esta funcion se usa para otener el total de comision de la agencia
	*/
	private function checkCommisionDefeated($invoice)
    {
        $response       = false;
        $commission     = $invoice->TotalCost;
        $commissionYear = substr($invoice->InvoiceDate, 0, 4);
        $yearCurrent    = date('Y');
        $isLess         = 0;

        if($commissionYear < $yearCurrent){
            $isLess = 1;
        }

        if( $isLess ){
            $commissionInvoice = $invoice->commissionInvoice;
            if($commission != 0) {
                if(trim($commissionInvoice) == ''){
                    $response = true;
                }
            }
        }

        return $response;
    }

	

}