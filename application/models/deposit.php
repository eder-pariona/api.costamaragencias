<?php

class Deposit extends CI_MODEL
{

	protected $_name = "deposit";

    public function __construct() {
        parent::__construct();
        $this->_db = $this->load->database('default', TRUE);
    }


    public function getDeposits($id, $from, $to, $amount)
    {
    	$query = "SELECT depdet.* FROM costamaragency.deposit dep 
			INNER JOIN costamaragency.depositDetail depdet
			ON dep.IdBankingMovement = depdet.IdBankingMovement
			WHERE dep.AccountID = '$id'
			AND dep.DateDeposit >= '$from' AND dep.DateDeposit <= '$to'";
        if ($amount) {
            $query .= "AND dep.Amount = $amount ";
        }
        
    	return $this->_db->query($query);
    }


}