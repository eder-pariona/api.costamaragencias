<?php 
class Gw_refund extends CI_Model
{
	public function __construct() {
        parent::__construct();
        $this->_db = $this->load->database('globalperu', TRUE);
    }

	public function ping()
	{
		echo "<pre>",__FILE__." on line ".__LINE__,": "; exit(); echo "</pre>"; 
	}

	public function getinvoices($parameters)
	{

		$query = "SELECT inv.invoiceDate,
						inv.invoiceNumber,
						inv.pnrLocator,
						inv.ticketNum,
						inv.bkAgt,
						ISNULL(acb.name,'No Ubicado') AS 'nameBkAgt',
						ISNULL(acb.addr2,'-') AS 'addr2BkAgt',
						ISNULL(acb.email,'-') AS 'emailBkAgt',
						inv.tktAgt,
						ISNULL(act.name,'No Ubicado') AS 'nameTktAgt',
						ISNULL(act.addr2,'-') AS 'dataTktAgt',
						ISNULL(act.email,'-') AS 'emailTktAgt'
				FROM dba.invoice inv WITH(NOLOCK)
				LEFT JOIN dba.employee emb WITH(NOLOCK) ON inv.bkagt = emb.sinecode
				LEFT JOIN dba.accountID acb WITH(NOLOCK) ON emb.accountID = acb.accountID
				LEFT JOIN dba.employee emt WITH(NOLOCK) ON inv.tktagt = emt.sinecode
				LEFT JOIN dba.accountID act WITH(NOLOCK) ON emt.accountID = act.accountID
				WHERE inv.status <> 'V'
				AND ISNULL(inv.docType,'') <> 'REF'
				AND inv.settle + inv.traveltype IN ('AA','IA')
				AND inv." . $parameters['where']; 
				// ticketNum = '6005728530'

		$response = $this->_db->query($query);
		$records  = $response->result();
		$this->_db->close();

		return $records;
	}

	public function getRefunds($parameters)
	{
		$query = "
			SELECT
				inv.accountId,
				inv.branch,
				invoiceDate = SUBSTRING(inv.invoicedate,1,10),
				apduedate = SUBSTRING(inv.ApDueDate,1,10),
				inv.invoiceNumber,
				inv.pnrLocator,
				inv.ticketNum,
				inv.traveler,
				inv.itinerary,
				inv.baseFare,
				taxes = (inv.Tax1 + inv.Tax2 +inv.Tax3+inv.Tax4),
				inv.Totalcost,
				inv.MiscCharge AS Penalidad,
				FP = (
				CASE WHEN	( inv.FOP= 'C' OR (ISNULL(inv.CCNumber, '') <> ''
												AND ( (inv.ccCompany = 'TP' AND left(inv.CCNumber,4) = '1220'
														AND right(inv.CCNumber,3) = '501')
													OR (inv.ccCompany = 'VI' AND left(inv.CCNumber,4) = '4919'
														AND right(inv.CCNumber,4) = '6358')
													OR (inv.ccCompany = 'AX' AND left(inv.CCNumber,4) = '3787'
														AND right(inv.CCNumber,3) = '001')
												)
											)
							) THEN 'C'
					ELSE 
						inv.FOP 
				END
				),
				formadepago = (
					CASE WHEN (FP = 'A') THEN 'Tarjeta de Credito'
						WHEN (FP = 'C') THEN 'Cash'
						WHEN (FP = 'P') THEN 'Tarjeta de Credito'
						ELSE 'Receivable' 
					END
				),
		        PorPagar = pay.CustRcvdAmt - pay.CustDueAmt,
				RefFee = ISNULL((SELECT fer.totalCost FROM dba.invoice fer WITH(NOLOCK) 
				                    WHERE fer.status <> 'V' AND fer.invoiceNumber = inv.invoiceNumber 
				    					AND fer.provider = 'SFEEREF' 
				    					AND fer.settle = 'I' AND fer.travelType = 'F' ),0.00),
				CantRef = ISNULL((SELECT COUNT(DISTINCT ref.ticketNum) FROM dba.invoice ref WITH(NOLOCK) 
				                	WHERE ref.status <> 'V' AND ref.invoiceNumber = inv.invoiceNumber 
									AND ISNULL(ref.docType,'') = 'REF' AND ref.ticketNum <> ''),0),
				Estado = (
					CASE WHEN (inv.settle = 'I' AND inv.travelType = 'A' AND ISNULL(inv.sort1,'') <> 'EMD') THEN (
							CASE WHEN (ISNULL((SELECT com.data FROM dba.comments com WITH(NOLOCK) 
							WHERE com.invPayid = inv.payID AND com.lineNum = 37),'') = 'NO PERMITE REEMBOLSO')
							THEN 'NO PERMITE REEMBOLSO'
							ELSE 'PENDIENTE' END
					)
					WHEN (inv.settle = 'I' AND inv.travelType = 'A' AND ISNULL(inv.sort1,'') = 'EMD' )
						THEN 'PAGADO CON EMD'
					WHEN (inv.settle = 'A' and inv.travelType = 'A')
						THEN (CASE WHEN (pay.CustRcvdAmt - pay.CustDueAmt = 0) THEN ( 
							CASE WHEN FP = 'P' THEN 'PAGADO A LA TARJETA' 
							ELSE 'PAGADO AL CLIENTE' 
							END
						)
						ELSE 'POR PAGAR AL CLIENTE' 
						END
						)
			   		END
		   		)
			FROM dba.invoice inv WITH(NOLOCK)
			INNER JOIN dba.payments pay with(nolock) ON pay.id = inv.payid
			WHERE inv.status <> 'V'
				AND isnull(inv.doctype,'') = 'REF' 
				AND inv.travelType = 'A'
				AND inv.settle in ('I', 'A')  
				AND inv.accountID <> 'REFCOSTA'
				-- AND inv.invoicedate between '2020-01-01' and '2020-03-27'
				AND inv.provider not in ('USA','77COSTAPER')
				AND inv." . $parameters['where']; 
				// -- AND inv.ticketNum = ? --búsqueda por nro de Ticket
		$response = $this->_db->query($query);
		$records  = $response->result();
		$this->_db->close();

		return $records;
	}


}
