<?php 
class Zdevtech extends CI_Model
{
	

	protected $api = 'https://api.zdev.tech/api-engine/engine';


	public function get($id)
    {
    	$code = 400;
	    $file_get_contents = @file_get_contents($this->api . '/' . $id);    	
	     if ($file_get_contents === FALSE) {
	        $message = "Cannot access '$this->api' to read contents.";
	    } else {
    		$code = 200;
	        $message = json_decode($file_get_contents, true);
	    }

	    return array(
	    	'code'    => $code,
	    	'message' => $message
	    );
    }



    public function save($request)
    {
    	$request = json_encode($request);
    	// $log = $this->logInsert($request, __FUNCTION__);
    	$response = $this->call('POST', $this->api . '/save', $request);
    	// $this->logUpdate($log['start'], $response, $log['logId']);
    	return $response;
    }


    
    public function call($method, $url, $data = NULL) 
    {
        $curl = curl_init($url);
        if($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
                    'Content-Type: application/json',                                                                                
                    'Content-Length: ' . strlen($data)
                    )                                                                    
                );
        }

        $result = curl_exec($curl);
        $response = json_decode($result, TRUE);
        curl_close($curl);
        return $response;
    }





}