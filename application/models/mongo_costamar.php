<?php
class Mongo_costamar extends CI_Model
{
	public function __construct() {
        parent::__construct();
    }

	public function ping()
	{
		echo "<pre>",__FILE__." on line ".__LINE__,": "; exit(); echo "</pre>"; 
	}


	/*
	 * credit :
	 * 1 : manual
	 * 2 : calculado
	 */
	public function find($request = array())
    {
    	$mongo_client = new \MongoClient();
		$database     = $mongo_client->costamar;
		$collection = $database->accounts;
		$response = $collection->findOne($request);
		
		return $response;
    }
}