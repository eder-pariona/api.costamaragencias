<?php 
class Gw_usa_refund extends CI_Model
{
	public function __construct() {
        parent::__construct();
        $this->_db = $this->load->database('globalusa', TRUE);
    }

	public function ping()
	{
		echo "<pre>",__FILE__." on line ".__LINE__,": "; exit(); echo "</pre>"; 
	}

	public function getinvoices($parameters)
	{
		
		$where = $parameters['where'];

		$query = "
			SELECT
				inv.invoiceDate, 
				inv.invoiceNumber,
				inv.pnrLocator, inv.ticketNum,
				inv.bkAgt,
				isnull(left(acb.name, length(acb.name)-4),'No Ubicado') AS 'nameBkAgt', 
				isnull(acb.HomePhone,'-') as 'whatsappBkAgt', 
				right(acb.name,4)  as 'anexoBkAgt',
				isnull(acb.BusPhone,'-') AS 'phoneBkAgt', 
				isnull(acb.email,'-') AS 'emailBkAgt',
				inv.tktAgt, 
				isnull(left(act.name, length(act.name)-4),'No Ubicado') AS 'nameTktAgt', 
				right(act.name,4) AS 'anexoTktAgt', 
				isnull(act.HomePhone,'-') AS 'whatsappTktAgt', 
				isnull(act.BusPhone,'-') AS 'phoneTktAgt',
				isnull(act.email,'-') AS 'emailTktAgt'
				from dba.invoice inv with(nolock)
				left join dba.employee emb with(nolock) on inv.bkagt = emb.sinecode
				left join dba.accountID acb with(nolock) on emb.accountID = acb.accountID
				left join dba.employee emt with(nolock) on inv.tktagt = emt.sinecode
				left join dba.accountID act with(nolock) on emt.accountID = act.accountID
				where
				inv.status <> 'V'
				and isnull(inv.docType,'') <> 'REF'
				and inv.settle + inv.traveltype in ('AA','IA')
				$where
		";


		$response = $this->_db->query($query);
		$records  = $response->result();
		$this->_db->close();

		return $records;
	}


	/*
	 * Query reembolsos USA
	 */
	public function getRefunds($parameters = array())
	{
		// echo "<pre>",__FILE__." on line ".__LINE__,": "; print_r($parameters); echo "</pre>"; 
		// echo "<pre>",__FILE__." on line ".__LINE__,": "; exit(); echo "</pre>"; 
		$accountId = $parameters['accountId'];
		$where     = $parameters['where'];
		$query = "
		SELECT
		    inv.accountid,
			inv.branch,
		    invoicedate = SUBSTRING(inv.invoicedate,1,10),
		    inv.InvoiceNumber,
		    inv.PnrLocator,
		    inv.TicketNum,
		    inv.Traveler,
		    inv.Itinerary,
		    inv.BaseFare,
		    Taxes = (inv.Tax1 + inv.Tax2 +inv.Tax3+inv.Tax4),
		    inv.Totalcost,
		    inv.MiscCharge AS Penalidad,
		    FP = (
		    CASE 
		    WHEN (
		        (inv.FOP = 'C' AND isnull(inv.SavingsCode,'') <> 'CNP')
		        OR (isnull(inv.CCNumber, '') <> ''
		            AND ((inv.ccCompany = 'VI' AND left(inv.CCNumber,4) = '4024' AND right(inv.CCNumber,4) = '3436')
		                OR (inv.ccCompany IN ('CA','VI') AND isnull(inv.SavingsCode,'') = 'VIR' )
		                )
		            )
		    ) THEN 'C'
		    WHEN (inv.FOP = 'A') THEN 'A'
		    ELSE 'P'
		    END),
		    formadepago = (
		        CASE WHEN (FP = 'A') THEN 'Tarjeta de Credito'
		        WHEN (FP = 'C') THEN 'Cash'
		        WHEN (FP = 'P') THEN 'Tarjeta de Credito'
		        ELSE 'Receivable'
		        END
		    ),
		    PorPagar = (
		        CASE WHEN (inv.FOP = 'C' or inv.FOP = 'A') THEN pay.CustRcvdAmt - pay.CustDueAmt
		        ELSE ISNULL((
		                    SELECT TOP 1 pnc.CustRcvdAmt - pnc.CustDueAmt 
		                    FROM dba.payments pnc WITH(nolock) 
		                    WHERE pnc.accountID = inv.accountID AND pnc.provider = inv.accountID 
		                    AND pnc.CustDueAmt = inv.totalCost 
		                    AND (pnc.[comment] LIKE '%' + inv.invoiceNumber 
		                        OR pnc.[comment] LIKE '%' + inv.ticketNum OR pnc.[comment] LIKE '%' + inv.pnrLocator)
		                    ),
		                    (
		                        CASE WHEN (FP = 'P')
		                           THEN 0.00
		                        ELSE
		                            inv.totalCost
		                        END
		                    )
		                )
		        END
		    ),
		    DevDscto = case when (inv.fop = 'P') then 0.00 else isnull((select dev.totalCost from dba.invoice dev with(nolock)
		                where dev.status <> 'V' AND dev.invoiceNumber = inv.invoiceNumber
		                and dev.provider = 'SFEE' and dev.settle + dev.travelType = 'IF'
		                and dev.itinerary = 'DEV. DSCTO.'),0.00) end,
		    RefFee = isnull((select fer.totalCost from dba.invoice fer with(nolock)
		                where fer.status <> 'V' and fer.invoiceNumber = inv.invoiceNumber
		                and fer.provider in ('890','SFEE') and fer.travelType = 'F'
		                and fer.itinerary = 'FEE REFUND'),0.00),
		    CantRef = isnull((select count(distinct ref.ticketNum) from dba.invoice ref with(nolock)
		                where ref.status <> 'V' and ref.invoiceNumber = inv.invoiceNumber
		                and isnull(ref.docType,'') = 'REF' and ref.ticketNum <> ''),0),
		    Estado = (
		        CASE WHEN (FP = 'P' and isnull(inv.SavingsCode,'') <> 'CNP' ) THEN 'TRAMITADO'
		            ELSE ( CASE WHEN (PorPagar <> 0 ) THEN 
		                (   CASE WHEN (inv.settle = 'I' and inv.provider <> 'TEN') THEN 'Pendiente de Pago'
		                    ELSE ( CASE WHEN (DATEDIFF(DAY,inv.invoiceDate,getdate())>= 10) THEN 'Pendiente de Pago'
		                           WHEN (DatePart(WeekDay, inv.invoiceDate) = 4) THEN (
		                                                                                CASE WHEN ( DATEDIFF(DAY,inv.invoiceDate,getdate()) >= 3 ) THEN 'Pendiente de Pago'
		                                                                                ELSE 'Pendiente de Respuesta de la Aerolinea'
		                                                                                END
		                                                                            )
		                        WHEN (DatePart(WeekDay, inv.invoiceDate) IN (3,2)) THEN
		                            (
		                                CASE WHEN ( DATEDIFF(DAY,inv.invoiceDate,getdate()) >= (DatePart(WeekDay, inv.invoiceDate)* (6-DatePart(WeekDay, inv.invoiceDate)) ) )
		                                THEN 'Pendiente de Pago'
		                                ELSE 'Pendiente de Respuesta de la Aerolinea'
		                                END
		                            )
		                        WHEN (DatePart(WeekDay, inv.invoiceDate) IN (5,6,7)) THEN 
		                            (
		                                CASE WHEN (DATEDIFF(DAY,inv.invoiceDate,getdate()) >= (DatePart(WeekDay, inv.invoiceDate) -1))
		                                THEN 'Pendiente de Pago'
		                                ELSE 'Pendiente de Respuesta de la Aerolinea'
		                                END
		                            )
		                        WHEN (DatePart(WeekDay, inv.invoiceDate) = 1) THEN
		                            (
		                                CASE WHEN ( DATEDIFF(DAY,inv.invoiceDate,getdate()) >= 7 )
		                                THEN 'Pendiente de Pago' 
		                                ELSE 'Pendiente de Respuesta de la Aerolinea'
		                                END
		                            )
		                            END
		                        )
		                    END
		                )
		            ELSE 'Pagado'
		            END
		            )
		        END)
		        FROM dba.invoice inv WITH(nolock) INNER JOIN dba.payments pay with(nolock)
		            ON pay.id = inv.payid WHERE inv.status <> 'V' AND isnull(inv.doctype,'') = 'REF' 
		            AND inv.travelType = 'A' AND inv.settle in ('I', 'A')
		            AND inv.accountid = '$accountId'
		            $where
		        AND (
		            inv.FOP <> 'C'
		            OR ( inv.FOP = 'C' AND inv.totalCost <> ISNULL(
		                                                    (SELECT reb.discount FROM dba.invoice reb WITH(nolock) WHERE reb.status <> 'V'
		                                                    AND reb.invoiceNumber = inv.invoiceNumber
		                                                    AND reb.settle + reb.travelType = 'IF'
		                                                    AND isnull(reb.doctype,'') <> 'REF'
		                                                    AND reb.discount = inv.totalCost )
		                                                    ,0.00)
		                )
		            )
		";

		$response = $this->_db->query($query);
		$records  = $response->result();
		$this->_db->close();

		return $records;

	}


}
