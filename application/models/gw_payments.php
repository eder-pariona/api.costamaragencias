<?php 
class Gw_payments extends CI_Model
{
	public function __construct() {
        parent::__construct();
        $this->_db = $this->load->database('globalperu', TRUE);
    }

	public function getByParameters($parameters)
	{
        $pnr   = $parameters['pnr'];
        $query = "
            SELECT 
                inv.accountId, 
                item = inv.SaleType+inv.Settle+inv.TravelType+inv.fop, 
                inv.pnrLocator, 
                inv.invoiceDate, 
                inv.invoiceNumber,
                inv.totalCost,
                debt = (
                        CASE WHEN (inv.CCNumber LIKE '378790_____2009' OR 
                            inv.CCNumber LIKE '122093_____1501' OR 
                            inv.CCNumber LIKE '491947_____7155' OR 
                            inv.CCNumber LIKE '378790_____1001' OR
                            inv.CCNumber LIKE '491947______5492') THEN 
                            ( SELECT (payc.CustDueAmt - payc.CustRcvdAmt) 
                                FROM dba.payments payc WHERE payc.provider = inv.accountID 
                            AND payc.[comment] LIKE inv.InvoiceNumber + '/' + inv.traveler + '%' AND payc.CustdueAmt = inv.TotalCost
                            )
                        ELSE 
                            (pay.CustDueAmt - pay.CustRcvdAmt)
                        END)
            FROM dba.Payments pay INNER JOIN dba.Invoice inv ON inv.PayID = pay.ID
            WHERE inv.PnrLocator = '$pnr'";

        $response = $this->_db->query($query);

        return $response->result_array();
    }
    
}