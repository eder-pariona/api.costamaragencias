<?php

class Account extends CI_Model
{

	protected $_name = "useragency";

    public function __construct() {
        parent::__construct();
        $this->_db = $this->load->database('default', TRUE);
    }


    public function getDetails($parameters)
    {
		
		$select = implode(', ', $parameters['columns']);
		$where  = array(
			'AccountID' => $parameters['id'],
			'Status' => 1
		);
        $query = $this->_db->select($select)
        			->from($this->_name)
        			->where($this->_name . '.AccountID', $parameters['id'])
        			->where($this->_name . '.Status', 1)
        			->join('ejecutive', $this->_name .'.EjecutiveID = ejecutive.EjecutiveID', 'LEFT')
        			->join('ejecutiveCtsCobrar as executive_sale', $this->_name .'.EjecutiveCtsCobrarID = executive_sale.EjecutiveID', 'LEFT')
        			->get();
        // $query = $this->_db->select($select)->where($where)->get($this->_name);

        return $query->row();
    }

}