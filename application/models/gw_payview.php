<?php
class Gw_payview extends CI_Model
{
	public function __construct() {
		parent::__construct();
		$this->_db = $this->load->database('globalperu', TRUE);
	}

	public function ping()
	{
	}


	/**
	 * Obtengo todas las deudas de la agencia
	 * Solo obtiene las deudas cash pendiente de pago
	 * Solo obtiene las deudas plastic pendiente de pago pero emitidas con las tarjetas de costamar (122093_1501, 378790_1001)
	 */
	public function getSumDebtsByAccountId($parameters = array())
	{
		$account_id     = $parameters['id'];
		$currentYear    = date('Y');
		$currentYearAnt = $currentYear - 2;

		$query = "
			SELECT  pv.AccountId,
				Debt= SUM(ISNULL(
					(CASE pv.Fop
						WHEN 'P' THEN (
							CASE
								WHEN ((SELECT TOP 1 invoi.InvoiceNumber FROM dba.Invoice invoi 
									WHERE (
									   invoi.CCNumber LIKE '378790_____2009' 
									OR invoi.CCNumber LIKE '122093_____1501' 
									OR invoi.CCNumber LIKE '491947_____7155' 
									OR invoi.CCNumber LIKE '378790_____1001'
									OR invoi.CCNumber LIKE '491947______5492'
									OR invoi.CCNumber LIKE '49191382____3727'
									 )
									AND invoi.InvoiceNumber = pv.InvoiceNumber) = pv.InvoiceNumber)
								 THEN
									(
										SELECT TOP 1 (invo.TotalCost - payc.CustRcvdAmt) FROM dba.payments payc 
										INNER JOIN dba.Invoice invo with(nolock) ON invo.AccountId = payc.Provider 
										WHERE payc.[comment] like pv.invoiceNumber + '/' + pv.traveler + '%'
										AND invo.fop = 'P' 
										AND invo.InvoiceNumber = pv.InvoiceNumber  
										AND payc.CustdueAmt = invo.TotalCost 
									)
								ELSE 0
							END
						)
						ELSE (pv.CustDueAmt-pv.CustRcvdAmt)
					 END), 0)
					)
				FROM dba.PayView pv WITH(nolock)
				WHERE pv.ACCOUNTID = '$account_id'
				AND (pv.STATUS <> 'V' OR pv.STATUS IS NULL) 
				AND YEAR (pv.custduedate) >= '$currentYearAnt'
				AND pv.provider NOT IN ('DCT', 'AGY', 'AGENCY', 'SAFETYPAY')
				AND pv.id NOT IN (SELECT inv.payid FROM dba.invoice inv WITH(nolock) WHERE inv.payid = pv.id 
					AND ISNULL(inv.docType,'') = 'REF'
				)
				AND pv.Branch NOT LIKE '77%'
				AND ((pv.[comment] LIKE pv.InvoiceNumber + '/' + pv.Traveler) OR pv.[comment] LIKE 'DEUDA%')
				AND pv.TotalCost > 0
				GROUP BY pv.AccountId";
			
			$response = $this->_db->query($query);

			return $response->row();
	}


	public function getSumDebtsByAccountIdDetail($parameters = array())
	{
		
		$account_id     = $parameters['id'];

		$query = "
			SELECT          
					pv.fop,
					pv.provider,
					pv.itinerary,
					pv.invoicenumber,
					pv.invoicedate,
					pv.ticketnum,
					pv.traveler,
					pv.id,
					pv.settle,
					pv.revtype,
					pv.branch,
					pv.CustDueAmt,
					pv.CustRcvdAmt,
					pv.custduedate,
					pnrlocator = ISNULL((SELECT pnrlocator FROM dba.invoice invo WHERE invo.payid = pv.id ), ''),
					Debt= (ISNULL(
									(CASE pv.Fop
										WHEN 'P' THEN (
											CASE
												WHEN ((SELECT TOP 1 invoi.InvoiceNumber FROM dba.Invoice invoi 
													WHERE (invoi.CCNumber LIKE '122093_____1501' 
													OR invoi.CCNumber LIKE '378790_____1001' 
													OR invoi.CCNumber LIKE '378790_____2009' 
													OR invoi.CCNumber LIKE '491947_____7155')
													AND invoi.InvoiceNumber = pv.InvoiceNumber) = pv.InvoiceNumber)
												 THEN
													(
														SELECT TOP 1 (invo.TotalCost - payc.CustRcvdAmt) FROM dba.payments payc 
														INNER JOIN dba.Invoice invo with(nolock) ON invo.AccountId = payc.Provider 
														WHERE payc.[comment] like pv.invoiceNumber + '/' + pv.traveler + '%'
														AND invo.fop = 'P' 
														AND invo.InvoiceNumber = pv.InvoiceNumber  
														AND payc.CustdueAmt = invo.TotalCost 
													)
												ELSE 0
											END
										)
										ELSE (pv.CustDueAmt-pv.CustRcvdAmt)
									 END), 
								0)
							)
					FROM dba.PayView pv WITH(nolock)
					WHERE pv.ACCOUNTID = '$account_id'
					AND (pv.STATUS <> 'V' OR pv.STATUS IS NULL) 
					AND YEAR (pv.custduedate)>= '2019'
					AND pv.provider NOT IN ('DCT', 'AGY', 'AGENCY', 'SAFETYPAY')
					AND pv.id NOT IN (
						SELECT inv.payid FROM dba.invoice inv WITH(NOLOCK) 
						WHERE inv.payid = pv.id 
							AND ISNULL(inv.docType,'') = 'REF'		
				   )
				   AND Debt > 0
				   AND pv.Branch NOT LIKE '77%'
				   AND ((pv.[comment] LIKE pv.InvoiceNumber + '/' + pv.Traveler) OR pv.[comment] LIKE 'DEUDA%')
		";
		$response = $this->_db->query($query);

		return $response->result_array();

	}


	/**
	 * Retorna los items provider AGY comisiones y fees propios de la agencia
	 * solo los que tienen factura de comision
	 */
	public function getItemsProviderAGY($parameters = array())
	{
		$query_where = '';
		if (isset($parameters['itinerary_equal'])) {
			$query_where = "AND inv.Itinerary LIKE '%" . $parameters['itinerary_equal'] . "%'";
		} 

		if (isset($parameters['itinerary_not_equal'])) {
			$query_where = "AND inv.Itinerary NOT LIKE '%" . $parameters['itinerary_not_equal'] . "%'";
		}

		$account_id      = $parameters['id'];
		$query = "
			SELECT amount = ISNULL(sum(pv.CustDueAmt - pv.CustRcvdAmt), 0.00),
			commentt = ISNULL((SELECT TOP 1 com.LineNum FROM dba.Comments com WHERE com.InvPayID = inv.PayID AND com.LineNum = 116 ), 0)
			FROM dba.PayView pv WITH(nolock)
			INNER JOIN dba.invoice inv WITH(nolock) ON pv.id = inv.payid
			WHERE ((pv.Status <> 'V') OR pv.invoiceDate IS NULL )
				AND pv.AccountID= '$account_id'
				AND pv.CustRcvdFlag<>'X'
				AND inv.saletype = 'R'
				AND inv.settle = 'I'
				AND inv.traveltype = 'F'
				AND inv.fop = 'C'
				AND inv.provider in ('AGY')
				AND inv.Branch NOT LIKE '77%'
				AND (inv.TicketNum LIKE 'FE-%' OR  inv.TicketNum LIKE 'FT-%')
				AND commentt <> 116
				AND pv.CustDueAmt-pv.CustRcvdAmt <> 0
				-- AND year(pv.CustDueDate) = DATEPART(YEAR, GETDATE())
		";
		$query    = $query . $query_where;
		$response = $this->_db->query($query)->result_array();
		
		$return 		= new stdClass;
		$return->amount = 0.00;

		if (is_array($response)) {
			$totals = [];
			foreach($response as $value) {
				$totals[] = $value['amount'];
			}
			$return->amount = array_sum($totals);
		}

		return $return;
	}

	/**
	 * Detalle de items con provider AGY
	 * Si una comision esta facturada tiene en su ticketnum el numero de factura
	 */
	public function getItemsProviderAGYDetail($parameters = array())
	{
		$account_id 			   = $parameters['id'];
		$query_where_ticketnum     = "(inv.TicketNum LIKE 'FE-%' OR  inv.TicketNum LIKE 'FT-%')";
		if (isset($parameters['ticketnum'])) {
			$query_where_ticketnum = "(inv.TicketNum NOT LIKE 'FT%' AND inv.TicketNum NOT LIKE 'FE%')";
		}
		$query_where_itinerary = '';
		if (isset($parameters['itinerary'])) {
			$query_where_itinerary = "AND inv.Itinerary LIKE '%" . $parameters['itinerary'] . "%'";
		} 

		if (isset($parameters['itinerary_not'])) {
			$query_where_itinerary = "AND inv.Itinerary NOT LIKE '%" . $parameters['itinerary_not'] . "%'";
		}

		$query = "
			SELECT
				inv.invoicedate,
				inv.itinerary,
				inv.invoicenumber,
				inv.apduedate,
				inv.pnrlocator,
				inv.TicketNum,
				inv.traveler,
				inv.provider,
				total = (pv.CustDueAmt - pv.CustRcvdAmt),
				pv.CustDueAmt,
				pv.CustRcvdAmt
			FROM dba.PayView pv WITH(nolock)
			INNER JOIN dba.invoice inv WITH(nolock) ON pv.id = inv.payid
			WHERE ((pv.Status <> 'V') OR pv.invoiceDate IS NULL )
				AND pv.AccountID= '$account_id'
				AND pv.CustRcvdFlag<>'X'
				AND inv.saletype = 'R'
				AND inv.settle = 'I'
				AND inv.traveltype = 'F'
				AND inv.fop = 'C'
				AND inv.provider in ('AGY')
				AND inv.Branch NOT LIKE '77%'
				AND $query_where_ticketnum
				AND pv.CustDueAmt-pv.CustRcvdAmt <> 0
				AND year(pv.CustDueDate) = DATEPART(YEAR, GETDATE())
		";
		$query    = $query . $query_where_itinerary;
		$response = $this->_db->query($query);
		$result   = $response->result_array();

		return $result;
	}


	/**
	 * 
	 */
	public function getItemsOnAccountNegative($parameters = array())
	{
		$account_id = $parameters['id'];
		$query = "
			SELECT DISTINCT
					pyo.id,
					amount = ISNULL((pyo.CustDueAmt - pyo.CustRcvdAmt), 0.00),
					pyo.[comment]
			FROM DBA.PayView pyo WITH(nolock) 
			INNER JOIN DBA.GlTransactions glt WITH(nolock) ON pyo.id = glt.payId 
				AND glt.status = 'O'
				AND glt.source IN ('R', 'A') 
				AND glt.Ref2 = '$account_id' 
				AND glt.Ref1 = 'ONACCOUNT' 
				WHERE (pyo.CustDueAmt - pyo.CustRcvdAmt) < 0 
			AND year(pyo.CustDueDate) <= DATEPART(YEAR, GETDATE())
			AND pyo.Branch LIKE '33%'
			AND pyo.CustDueDate >= '2020-06-01'
			AND ISNULL(glt.CheckNum,'jenny') NOT LIKE '1%'
			AND pyo.[Comment] not LIKE '%**/%'
		";
		if (isset($parameters['post_date'])) {
			$query .= " AND glt.PostDate = ' " . $parameters['post_date'] . "'";
		}

		$response     = $this->_db->query($query);
		$result_array = $response->result_array();	
		
		return $result_array;
	}

	// temporal, xq esta api se dara de baja
	public function getOnAccountNegatives($parameters = array())
	{
		$account_id = $parameters['id'];
		$query = "
				SELECT
				pv.id,
				pv.AccountId,
				pv.CustRcvdFlag,
				pv.CustDueAmt,
				pv.CustRcvdAmt,
				amount = pv.CustDueAmt - pv.CustRcvdAmt,
				pv.[Comment]
				
			FROM dba.PayView pv WITH(NOLOCK) 
			WHERE pv.AccountID= '$account_id'   
			AND pv.CustDueDate >= '2020-06-01' 
			AND pv.CustRcvdFlag = 'N' 
			AND pv.Source = 'M' 
			AND pv.[Comment] NOT LIKE '%**/%' 
			AND pv.CustDueAmt - pv.CustRcvdAmt < 0 
			AND (
				SELECT TOP 1 inv.PayID from dba.Invoice inv WITH(NOLOCK) 
				WHERE inv.InvoiceNumber + '/' + inv.Traveler  = pv.[Comment]
				AND inv.AccountId = pv.AccountId
				) IS NULL
		";
		$response     = $this->_db->query($query);
		$result_array1 = $response->result_array();
				
		$query = "
			SELECT
				pv.id,
				pv.AccountId,
				pv.CustRcvdFlag,
				pv.CustDueAmt,
				pv.CustRcvdAmt,
				amount = pv.CustDueAmt - pv.CustRcvdAmt,
				pv.[Comment]
			FROM dba.PayView pv WITH(NOLOCK) 
			WHERE pv.AccountID= '$account_id'  
			AND pv.CustDueDate >= '2020-06-01' 
			AND pv.CustRcvdFlag = 'N' 
			AND pv.Source = 'M' 
			AND pv.[Comment] NOT LIKE '%**/%' AND pv.CustDueAmt - pv.CustRcvdAmt < 0 
			AND (
				SELECT TOP 1 debt = (pay.CustDueAmt - pay.CustRcvdAmt) 
					FROM dba.Invoice inv WITH(NOLOCK)
						INNER JOIN dba.Payments pay ON inv.AccountId = pay.Provider 
							WHERE inv.InvoiceNumber = SUBSTRING( pv.[Comment], 1, 9 ) 
								AND pay.[Comment] = pv.[Comment] 
								AND inv.Fop = 'P' 
								AND debt = 0
								AND ISNULL(inv.DocType, '') <> 'REF'
						) IS NOT NULL
			";
			
		$response      = $this->_db->query($query);
		$result_array2 = $response->result_array();
	    $response      = array_merge($result_array1, $result_array2);

		return $response;
	}


}