<?php

class Modulocobranzas_user extends CI_Model
{

	protected $_name = "users";

    public function __construct() {
        parent::__construct();
        $this->_db = $this->load->database('modulocobranzas', TRUE);
    }


    public function get_account_executives($parameters)
    {
        $query = $this->_db->select('au.account_id as AccountID, 
        					au.user_id as executive_sale_id, 
        					u2.name as executive_sale_name, 
        					u1.name as executive_name,
        					uhu.user_executive_accounting_id as executive_id')
        			->from('accounts_users as au')
        			->where('au.account_id', $parameters['id'])
        			->join('users as u2', 'u2.id = au.user_id')
        			->join('users_has_users as uhu', 'uhu.user_executive_sales_id = au.user_id')
        			->join('users as u1', 'u1.id = uhu.user_executive_accounting_id')
        			->get();

        return $query->row();
    }



}